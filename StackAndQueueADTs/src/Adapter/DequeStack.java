package Adapter;
import Interface.*;
import Stack.ArrayStack;
import Stack.LinkedListStack;;
/**
 * This Generic adapter class extends stack class and contains various methods for 
 * manipulating stack using Generic Array and Linked List.
 *  @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public class DequeStack<E> implements Stack<E>  {
	//Declarations
	Deque<E> stack;
	/**
	 * Initializes an empty array of fixed size
	 * @param arrayDeque - array initialization
	 */
	public DequeStack(ArrayStack<E> arrayStack) { 
		this.stack =  (Deque<E>) arrayStack; 
	}
	/**
	 * Initializes LinkedList stack
	 * @param linkedListDeque - linkedListDeque initialization
	 */
	public DequeStack(LinkedListStack<E> linkedListStack) {
		this.stack = linkedListStack;
	}	
	/**
	 * Returns the size of an array
	 * @return Returns the size of an array
	 */
	@Override
	public int size() { 
		return stack.size(); 
	}
	/**
	 * Checks for an empty array
	 * @return True - if empty else false
	 */
	@Override
	public boolean isEmpty() { 
		return stack.isEmpty(); 
	}
	/**
	 * Returns top element of a stack
	 * @return Top element of a stack
	 */
	public E top() { 
		return stack.firstElement(); 
	}
	/*Removes last element of a stack
	 * @return Last element of a stack
	 * (non-Javadoc)
	 * @see java.util.Stack#pop()
	 */
	@Override
	public E pop() { 
		return stack.removeFirst(); 
	}	
	/*Returns array as string 
	 * @return Returns array content
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return stack.toString();
	}
	/*
	 * Push element into stack
	 * @param element to be pushed
	 * (non-Javadoc)
	 * @see Interface.Stack#push(java.lang.Object)
	 */
	@Override
	public void push(E element) {
		stack.insertFirst(element);
	}
}
