package Adapter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import Interface.Deque;
import Queue.*;
/**
 * This Generic adapter class implements queue interface and contains various methods for 
 * manipulating Deque using Generic Array and Linked List (such as two way insert and delete).
 *  @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public class DequeQueue<E>  implements Queue<E> {
	//Declarations
	Deque<E> deque;
	/**
	 * Initializes an empty array of fixed size
	 * @param arrayDeque - array initialization
	 */
	public DequeQueue(ArrayDeque<E> arrayDeque) { 
		this.deque =  (Deque<E>) arrayDeque; 
	}	
	/**
	 * Initializes LinkedListDeque
	 * @param linkedListDeque - linkedList Deque initialization
	 */
	public DequeQueue(LinkedListDeque<E> linkedListDeque) {
		this.deque = linkedListDeque;
	}	
	/**
	 * Returns the size of an array
	 * @return Returns the size of an array
	 */
	@Override
	public int size() { 
		return deque.size(); 
	}
	/**
	 * Checks for an empty array
	 * @return True - if empty else false
	 */
	@Override
	public boolean isEmpty() { 
		return deque.isEmpty(); 
	}
	/**
	 * Inserts an element at front of deque
	 * @param element - data to be added
	 * @throws FullStructureException when deque is full
	 */
	public void insertFirst(E element) { 
		deque.insertFirst(element); 
	}
	/**
	 * Inserts an element at rear end of the deque
	 * @param element - data to be added
	 */
	public void insertLast(E element) { 
		deque.insertLast(element); 
	}
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 */
	public E removeLast() { 
		return deque.removeLast(); 
	}
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	public E removeFirst() { 
		return deque.removeFirst(); 
	}
	/**
	 * Returns the array content 
	 * @return Returns the array content 
	 */
	@Override
	public String toString() {
		return deque.toString();
	}
	/**
	 * Returns head of the deque
	 * @param Return head of the deque
	 */
	@Override
	public E peek() {
		return deque.firstElement();
	}
	/**
	 * Returns tail of the deque
	 * @param Return tail of the deque
	 */
	public E peekLast() {
		return deque.lastElement();
	}
	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean addAll(Collection<? extends E> c) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void clear() {
		// TODO Auto-generated method stub
	}
	@Override
	public boolean add(E e) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean offer(E e) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public E remove() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E poll() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E element() {
		// TODO Auto-generated method stub
		return null;
	}	
}
