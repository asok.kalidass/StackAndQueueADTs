package Interface;
import Exceptions.EmptyStructureException;
import Exceptions.FullStructureException;
/**
 * The Deque interface contains methods to access/remove an element in a deque
 * Implementation of deque has no limitation on the type and size of an array
 *  @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public interface Deque<E> {
	/**
	 * Returns the size of an array
	 */
	public int size();
	/**
	 * Checks for an empty array
	 */
	public boolean isEmpty();
	/**
	 * Inserts an element at front of deque
	 * @param element - data to be added
	 */
	public void insertFirst(E element);
	/**
	 * Inserts an element at rear end of the deque
	 * @param element - data to be added
	 * @throws FullStructureException when deque is full
	 */
	public void insertLast(E element);
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	public E removeFirst() throws EmptyStructureException;
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	public E removeLast() throws EmptyStructureException;
	/**
	 * Returns First element of deque
	 * @return Return First element of deque
	 */
	public E firstElement() throws EmptyStructureException;
	/**
	 * Returns last element of deque
	 * @return Return last element of deque
	 */
	public E lastElement() throws EmptyStructureException;
}
