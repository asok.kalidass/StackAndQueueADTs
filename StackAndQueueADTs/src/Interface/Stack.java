package Interface;
/**
 * The Stack interface contains methods to access/remove an element in a stack data structure
 * Implementation of stack has no limitation on the type and size of an array
 *  @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public interface Stack<E> {
	/**
	 * Returns the size of an array
	 */
	public int size();
	/**
	 * Checks for an empty array
	 */
	public boolean isEmpty();
	/**
	 * Returns top element of a stack
	 * @return Top element of a stack
	 */
	public E top();
	/**
	 * Returns top element of a stack
	 * @return Top element of a stack
	 */
	public E pop();
	/**
	 * push element to a stack
	 */
	public void push(E element);
	/**
	 * Returns string of a class object
	 * @returns Returns string of a class object
	 */
	public String toString();
}
