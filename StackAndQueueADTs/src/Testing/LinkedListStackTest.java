package Testing;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import Adapter.DequeStack;
import Exceptions.EmptyStructureException;
import Stack.LinkedListStack;
/**
 *Represents JUNIT test class for linked list implementation of stack
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class LinkedListStackTest {
	/**
     * A test to push elements into stack 
     */
	@Test
	public void TestPushArrayStack() {
		DequeStack<Integer> stackList = new DequeStack<Integer>(new LinkedListStack<Integer>());
		stackList.push(1);
		stackList.push(2);
		stackList.push(3);
		stackList.push(4);
		stackList.push(5);
		stackList.push(6);
		int topElement = stackList.top();
		assertEquals(6,topElement);
		System.out.println(stackList.toString());
		assertEquals(6, stackList.size());
	}	
	/**
	 * A test to fill array stack and pop it till it gets empty
	 */
	@Test
	public void TestFillAndEmptyArrayStack() {
		DequeStack<Integer> stackList = new DequeStack<Integer>(new LinkedListStack<Integer>());
		stackList.push(1);
		stackList.push(2);
		stackList.push(3);
		stackList.push(4);
		stackList.push(5);
		stackList.pop();
		int popedElement = stackList.pop();
		assertEquals(4, popedElement);
		stackList.pop();
		stackList.pop();
		stackList.pop();
		System.out.println(stackList.toString());
		assertEquals(0, stackList.size());
	}
	/**
	 * A test to fill array stack and pop it till it gets empty
	 */
	@Test
	public void TestFillEmptyAndFillArrayStack() {
		DequeStack<Integer> stackList = new DequeStack<Integer>(new LinkedListStack<Integer>());
		stackList.push(1);
		stackList.push(2);
		stackList.push(3);
		stackList.push(4);
		stackList.push(5);
		stackList.pop();
		stackList.pop();
		stackList.pop();
		stackList.pop();
		stackList.pop();
		stackList.push(1);
		System.out.println(stackList.toString());
		stackList.pop();
		System.out.println(stackList.toString());
		stackList.push(1);
		System.out.println(stackList.toString());
		assertEquals(1, stackList.size());
	}
	/**
	 * A test for empty stack exception
	 */
	@Test(expected = EmptyStructureException.class)
	public void TestForEmptyArrayStack() {
		DequeStack<Integer> stackList = new DequeStack<Integer>(new LinkedListStack<Integer>());
		stackList.push(1);
		stackList.push(2);		
		stackList.pop();
		stackList.pop();
		stackList.pop();				
	}
	/**
	 * A test to find top element in a stack
	 */
	@Test
	public void TestForTopArrayStack() {
		DequeStack<Integer> stackList = new DequeStack<Integer>(new LinkedListStack<Integer>());
		stackList.push(1);
		stackList.push(2);
		stackList.push(3);
		stackList.push(4);
		stackList.push(5);
		int top1 = stackList.top();
		assertEquals(5, top1);
		stackList.pop();
		stackList.pop();
		int top2 = stackList.top();
		assertEquals(3, top2);
		stackList.push(1);
		stackList.pop();
		stackList.push(1);
		int top3 = stackList.top();
		assertEquals(1, top3);
		System.out.println(stackList.toString());
		assertEquals(4, stackList.size());
	}	
}
