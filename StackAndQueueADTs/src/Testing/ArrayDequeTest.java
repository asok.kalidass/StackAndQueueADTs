package Testing;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import Adapter.DequeQueue;
import Exceptions.*;
import Queue.ArrayDeque;
/**
 *Represents JUNIT test class for array implementation of Deque
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class ArrayDequeTest {
    /**
     * A test to insert elements through front twice when front index is at initial position 
     */
	@Test(expected=FullStructureException.class)
	public void TestInserFirstDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertFirst(1);
		dequeueArray.insertFirst(2);
	}
	/**
	 * A test to fill Dequeue through inserting at  its rear end
	 */
	@Test
	public void TestInserLastDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.insertLast(4);
		dequeueArray.insertLast(5);
		System.out.println(dequeueArray.toString());
		assertEquals(arraySize, dequeueArray.size());
	}
	/**
	 * A test to overload the array elements through insert at its rear end
	 */
	@Test(expected=FullStructureException.class)
	public void TestExtraInserLastDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.insertLast(4);
		dequeueArray.insertLast(5);
		dequeueArray.insertLast(6);		
	}
	/**
	 * A test to insert array elements through rear end and empty its content from front 
	 */
	@Test
	public void TestEmptyFrmFirstDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.insertLast(4);
		dequeueArray.insertLast(5);
		int removedElement = dequeueArray.removeFirst();
		assertEquals(1, removedElement);
		dequeueArray.removeFirst();
		dequeueArray.removeFirst();
		dequeueArray.removeFirst();
		dequeueArray.removeFirst();
		System.out.println(dequeueArray.toString());
		assertEquals(0, dequeueArray.size());
	}
	/**
	 * A test to check is array empty before removing the array content
	 */
	@Test(expected=EmptyStructureException.class)
	public void TestEmptyFrmLastDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.insertLast(4);
		dequeueArray.insertLast(5);
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
	}
	/**
	 * A test to insert 1st array elements from front & rest from rear then empty its content from its rear end
	 */
	@Test
	public void TestInsertFirstAndRmvFrmLastDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertFirst(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.insertLast(4);
		dequeueArray.insertLast(5);
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		int removedElement = dequeueArray.removeLast();
		assertEquals(1, removedElement);
		System.out.println(dequeueArray.toString());
		assertEquals(0, dequeueArray.size());
	}
	/**
	 * A test for random insert and delete operation
	 */
	@Test
	public void TestDeleteAndAddDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		dequeueArray.insertFirst(8);
		dequeueArray.removeLast();
		dequeueArray.removeFirst();
		dequeueArray.insertFirst(11);
		dequeueArray.insertLast(12);
		System.out.println(dequeueArray.toString());
		assertEquals(2, dequeueArray.size());
	}
	/**
	 * A test for last element deletion at first and Is array empty Test
	 */
	@Test(expected=EmptyStructureException.class)
	public void TestLastElementDeletionDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.removeFirst();
		dequeueArray.removeFirst();
	}
	/**
	 * A test for last element deletion in mid
	 */
	@Test
	public void TestMidElementDeletionDequeQueue() {
		int arraySize = 5;
		DequeQueue<Integer> dequeueArray = new DequeQueue<Integer>(new ArrayDeque<Integer>(arraySize));
		dequeueArray.insertLast(1);
		dequeueArray.insertLast(2);
		dequeueArray.insertLast(3);
		dequeueArray.insertLast(4);
		dequeueArray.insertLast(5);
		System.out.println(dequeueArray.toString());
		dequeueArray.removeFirst();
		dequeueArray.removeFirst();
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		System.out.println(dequeueArray.toString());
		dequeueArray.removeLast();
		System.out.println(dequeueArray.toString());
		assertEquals(0, dequeueArray.size());		
	}
	/**
	 * A test for last element deletion in mid
	 */
	@Test
	public void TestMidElementDeletionStringDequeQueue() {
		int arraySize = 5;
		DequeQueue<String> dequeueArray = new DequeQueue<String>(new ArrayDeque<String>(arraySize));
		dequeueArray.insertLast("A");
		dequeueArray.insertLast("B");
		dequeueArray.insertLast("C");
		dequeueArray.insertLast("D");
		dequeueArray.insertLast("E");
		System.out.println(dequeueArray.toString());
		dequeueArray.removeFirst();
		String removeLast = dequeueArray.removeFirst();
		assertEquals("B", removeLast);	
		dequeueArray.removeLast();
		dequeueArray.removeLast();
		System.out.println(dequeueArray.toString());
		dequeueArray.removeLast();
		System.out.println(dequeueArray.toString());
		assertEquals(0, dequeueArray.size());		
	}
}
