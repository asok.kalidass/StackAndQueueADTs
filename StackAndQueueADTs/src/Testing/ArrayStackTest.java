package Testing;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import Adapter.DequeStack;
import Exceptions.*;
import Stack.ArrayStack;
/**
 *Represents JUNIT test class for array implementation of stack
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class ArrayStackTest {
	/**
     * A test to push elements into stack and make it over flow
     */
	@Test(expected=FullStructureException.class)
	public void TestPushArrayStack() {
		int arraySize = 5;
		DequeStack<Integer> stackArray = new DequeStack<Integer>(new ArrayStack<Integer>(arraySize));
		stackArray.push(1);
		stackArray.push(2);
		stackArray.push(3);
		stackArray.push(4);
		stackArray.push(5);
		stackArray.push(6);
	}
	/**
	 * A test to fill stack 
	 */
	@Test
	public void TestFillArrayStack() {
		int arraySize = 6;
		DequeStack<Integer> stackArray = new DequeStack<Integer>(new ArrayStack<Integer>(arraySize));
		stackArray.push(1);
		stackArray.push(2);
		stackArray.push(3);
		stackArray.push(4);
		stackArray.push(5);
		stackArray.push(6);
		System.out.println(stackArray.toString());
		assertEquals(arraySize, stackArray.size());
	}
	/**
	 * A test to fill array stack and pop it till it gets empty
	 */
	@Test
	public void TestFillAndEmptyArrayStack() {
		int arraySize = 5;
		DequeStack<Integer> stackArray = new DequeStack<Integer>(new ArrayStack<Integer>(arraySize));
		stackArray.push(1);
		stackArray.push(2);
		stackArray.push(3);
		int topElement = stackArray.top();
		assertEquals(3, topElement);
		stackArray.push(4);
		stackArray.push(5);
		stackArray.pop();
		int popedElement = stackArray.pop();
		assertEquals(4, popedElement);
		stackArray.pop();
		stackArray.pop();
		stackArray.pop();
		System.out.println(stackArray.toString());
		assertEquals(0, stackArray.size());
	}
	/**
	 * A test to fill array stack and pop it till it gets empty
	 */
	@Test
	public void TestFillEmptyAndFillArrayStack() {
		int arraySize = 5;
		DequeStack<Integer> stackArray = new DequeStack<Integer>(new ArrayStack<Integer>(arraySize));
		stackArray.push(1);
		stackArray.push(2);
		stackArray.push(3);
		stackArray.push(4);
		stackArray.push(5);
		stackArray.pop();
		stackArray.pop();
		stackArray.pop();
		stackArray.pop();
		int popedElement = stackArray.pop();
		assertEquals(1, popedElement);
		stackArray.push(1);
		System.out.println(stackArray.toString());
		stackArray.pop();
		System.out.println(stackArray.toString());
		stackArray.push(1);
		System.out.println(stackArray.toString());
		assertEquals(1, stackArray.size());
	}
	/**
	 * A test for empty stack exception
	 */
	@Test(expected = EmptyStructureException.class)
	public void TestForEmptyArrayStack() {
		int arraySize = 5;
		DequeStack<Integer> stackArray = new DequeStack<Integer>(new ArrayStack<Integer>(arraySize));
		stackArray.push(1);
		stackArray.push(2);		
		stackArray.pop();
		stackArray.pop();
		stackArray.pop();				
	}
	/**
	 * A test to find top element in a stack
	 */
	@Test
	public void TestForTopArrayStack() {
		int arraySize = 5;
		DequeStack<Integer> stackArray = new DequeStack<Integer>(new ArrayStack<Integer>(arraySize));
		stackArray.push(1);
		stackArray.push(2);
		stackArray.push(3);
		stackArray.push(4);
		stackArray.push(5);
		int top1 = stackArray.top();
		assertEquals(5, top1);
		stackArray.pop();
		stackArray.pop();
		int top2 = stackArray.top();
		assertEquals(3, top2);
		stackArray.push(1);
		stackArray.pop();
		stackArray.push(1);
		int top3 = stackArray.top();
		assertEquals(1, top3);
		System.out.println(stackArray.toString());
		assertEquals(4, stackArray.size());
	}	
}
