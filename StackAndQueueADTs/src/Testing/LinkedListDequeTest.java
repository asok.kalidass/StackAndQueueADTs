package Testing;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import Adapter.DequeQueue;
import Exceptions.EmptyStructureException;
import Queue.LinkedListDeque;;
/**
 *Represents JUNIT test class for Linked List implementation of Deque
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class LinkedListDequeTest {
	/**
	 * A test to insert elements through front twice when front index is at initial position 
	 */
	@Test
	public void TestInserFirstDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertFirst(1);
		dequeueList.insertFirst(2);	
		dequeueList.insertFirst(3);
		System.out.println(dequeueList.toString());
		assertEquals(3, dequeueList.size());
	}
	/**
	 * A test to fill Dequeue through inserting at  its rear end
	 */
	@Test
	public void TestInserLastDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertFirst(1);
		dequeueList.insertFirst(2);	
		dequeueList.insertFirst(3);
		dequeueList.insertLast(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		int removedElement = dequeueList.removeFirst();
		assertEquals(3, removedElement);
		dequeueList.removeLast();
		System.out.println(dequeueList.toString());
		assertEquals(4, dequeueList.size());
	}
	/**
	 * A test to overload the array elements through insert at its rear end
	 */
	@Test
	public void TestExtraInserLastDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertLast(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		dequeueList.insertLast(4);
		dequeueList.insertLast(5);
		dequeueList.insertLast(6);
		System.out.println(dequeueList.toString());
		assertEquals(6, dequeueList.size());
	}
	/**
	 * A test to check deletion at its rear end on empty list 
	 */
	@Test(expected = EmptyStructureException.class)
	public void TestForEmptyListAtLastDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.removeLast();		
	}
	/**
	 * A test to check deletion at its front end on empty list 
	 */
	@Test(expected = EmptyStructureException.class)
	public void TestForEmptyListAtFirstDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.removeFirst();		
	}
	/**
	 * A test to insert array elements through rear end and empty its content from front 
	 */
	@Test
	public void TestEmptyFrmFirstDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertLast(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		dequeueList.insertLast(4);
		dequeueList.insertLast(5);
		dequeueList.removeFirst();
		dequeueList.removeFirst();
		dequeueList.removeFirst();
		int removedElement = dequeueList.removeFirst();
		assertEquals(4, removedElement);
		dequeueList.removeFirst();
		System.out.println(dequeueList.toString());
		assertEquals(0, dequeueList.size());
	}
	/**
	 * A test to check is array empty before removing the array content
	 */
	@Test(expected=EmptyStructureException.class)
	public void TestEmptyFrmLastDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertLast(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		dequeueList.insertLast(4);
		dequeueList.insertLast(5);
		dequeueList.removeLast();
		dequeueList.removeLast();
		dequeueList.removeLast();
		dequeueList.removeLast();
		dequeueList.removeLast();
		dequeueList.removeLast();
	}
	/**
	 * A test to insert 1st array elements from front & rest from rear then empty its content from its rear end
	 */
	@Test
	public void TestInsertFirstAndRmvFrmLastDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertFirst(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		dequeueList.insertLast(4);
		dequeueList.insertLast(5);
		int removedElement = dequeueList.removeFirst();
		assertEquals(1, removedElement);
		dequeueList.removeLast();
		dequeueList.removeLast();
		dequeueList.removeLast();
		dequeueList.removeLast();
		System.out.println(dequeueList.toString());
		assertEquals(0, dequeueList.size());
	}
	/**
	 * A test for random insert and delete operation
	 */
	@Test
	public void TestDeleteAndAddDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertLast(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		dequeueList.removeLast();
		int removedElement = dequeueList.removeLast();
		assertEquals(2, removedElement);
		dequeueList.insertFirst(8);
		dequeueList.removeLast();
		dequeueList.removeFirst();
		dequeueList.insertFirst(11);
		dequeueList.insertLast(12);
		System.out.println(dequeueList.toString());
		assertEquals(2, dequeueList.size());
	}
	/**
	 * A test for last element deletion at first and Is array empty Test
	 */
	@Test(expected=EmptyStructureException.class)
	public void TestLastElementDeletionDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertLast(1);
		dequeueList.removeFirst();
		dequeueList.removeFirst();
	}
	/**
	 * A test for last element deletion in mid
	 */
	@Test
	public void TestMidElementDeletionDequeQueue() {
		DequeQueue<Integer> dequeueList = new DequeQueue<Integer>(new LinkedListDeque<Integer>());
		dequeueList.insertLast(1);
		dequeueList.insertLast(2);
		dequeueList.insertLast(3);
		dequeueList.insertLast(4);
		dequeueList.insertLast(5);
		System.out.println(dequeueList.toString());
		dequeueList.removeFirst();
		dequeueList.removeFirst();
		dequeueList.removeLast();
		dequeueList.removeLast();
		System.out.println(dequeueList.toString());
		dequeueList.removeLast();
		System.out.println(dequeueList.toString());
		assertEquals(0, dequeueList.size());		
	}
	/**
	 * A test for last element deletion in mid
	 */
	@Test
	public void TestStringDequeQueue() {
		DequeQueue<String> dequeueList = new DequeQueue<String>(new LinkedListDeque<String>());
		dequeueList.insertLast("ASOK");
		dequeueList.insertLast("K");
		dequeueList.insertLast("DASS");
		dequeueList.insertLast("DAL");
		dequeueList.insertLast("GRAD STUDENT");
		System.out.println(dequeueList.toString());
		dequeueList.removeFirst();
		dequeueList.removeFirst();
		String removedElement = dequeueList.removeLast();
		assertEquals("GRAD STUDENT", removedElement);
		dequeueList.removeLast();
		System.out.println(dequeueList.toString());
		dequeueList.removeLast();
		System.out.println(dequeueList.toString());
		assertEquals(0, dequeueList.size());		
	}
}
