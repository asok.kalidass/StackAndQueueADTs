package Queue;
import Exceptions.EmptyStructureException;
import Interface.Deque;
/**
 * This Generic class implements Deque interface and contains various methods for manipulating Deque using Generic Array (such as
 * two way insert and delete).
 * <p>
 * Implementation of Deque may have following Special Case
 * <ul>
 * <li>First Element Insertion - Both Front and Rear points to same node
 * <li>Empty Array - Denoted by size zero
 * <li>When Rear and Front pointer points to same index it implies list has only one value
 * </ul>
 * <p>
 * Assumptions/Restrictions: User inputs valid single dimension array of allowable length
 * <p>
 * Noteworthy Features: 
 * <ul>
 * <li>Generic Implementation of linked list
 * <li>Tested using Junit for various scenarios 
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public class LinkedListDeque<E> implements Deque<E> {
	//Declarations
	int size = 0;
	private Node<E> front, rear;

	/**
	 * Initializes an Linked Lists
	 * @param maxSize
	 */
	public LinkedListDeque() {	

	}
	/**
	 * This class represent the nodes of a linked list
	 *
	 * @param <E> Element to be inserted to a list
	 */
	@SuppressWarnings("hiding")
	class Node<E>
	{ 
		//Declarations
		E item;
		Node<E> next;
		/*
		 * Initializes a Node 
		 */
		Node() {

		}
		/*
		 * Gets the value form the node
		 * @param items - the string builder initialization 
		 * @return returns the node item
		 */
		public E getItem(StringBuilder items)
		{
			return item;
		}
	}
	/**
	 * Returns the size of an array
	 * @return Returns the size of an array
	 */
	@Override
	public int size() {		
		return size;
	}
	/**
	 * Checks for an empty array
	 * @return True - if empty else false
	 */
	@Override
	public boolean isEmpty() {			
		//0 denotes the size variable is not manipulated at any of the below methods implying there are no elements in an array)
		if ((size == 0)) {
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Inserts an element at front of deque
	 * @param element - data to be added
	 */
	@Override
	public void insertFirst(E element) {
		Node<E> frontElement = new Node<E>();
		frontElement.item = element;
		frontElement.next = front;
		front = frontElement;
		//If list is empty point the pointers to same node
		if(isEmpty()) {
			front = rear = frontElement;				
		}			
		size++;
	}
	/**
	 * Inserts an element at rear end of the deque
	 * @param element - data to be added
	 */
	@Override
	public void insertLast(E element) {	
		Node<E> lastElement = new Node<E>();
		lastElement.item = element;
		//If list is empty point the pointers to same node
		if (isEmpty()) {
			front = rear = lastElement;				
		}	
		else {
			rear.next = lastElement;
			rear = lastElement;
		}
		size++;
	}
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	@Override
	public E removeFirst() throws EmptyStructureException {
		Node<E> remapFirstNode = front;
		//If list is empty point reset the pointers to its initial state
		if (isEmpty()) { front = rear = null; }
		if (front != null) {			
			front = remapFirstNode.next;
			size--;
		}
		else {
			throw new EmptyStructureException("Atleast one item is required to perform deletion operation");
		}
		return remapFirstNode.item;
	}	
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	@Override
	public E removeLast() throws EmptyStructureException {
		Node<E> queue = front;
		Node<E> lastNode = front;
		E deletedItem = null;
		int count = size;
		if (front != null && size > 0) {	
			//Traversing through the list and re-mapping the last reference
			while(queue != null) {
				if (count == 2) {
					lastNode = queue;
				}
				deletedItem = queue.item;
				queue = queue.next;
				count--;
			}			
			lastNode.next = null;
			size--;
			//If list is empty point reset the pointers to its initial state
			if(isEmpty()) { front = rear = null; }
		}
		else {
			throw new EmptyStructureException("Atleast one item is required to perform deletion operation");
		}
		return deletedItem;
	}
	/**
	 * Returns the node content 
	 * @return Returns the node content 
	 */
	@Override
	public String toString() {
		return readQueue();
	}
	/**
	 * Returns First element of deque
	 * @return Return First element of deque
	 */
	@Override
	public E firstElement() throws EmptyStructureException {
		//Always front points to head position of deque
		Node<E> frontNode = front ;
		return frontNode.item;
	}
	/**
	 * Returns last element of deque
	 * @return Return last element of deque
	 */
	@Override
	public E lastElement() throws EmptyStructureException {
		//Always rear points to tail position of deque
		Node<E> lastNode = rear ;
		return lastNode.item;
	}
	/*
	 * Reads the queue
	 */
	public String readQueue()
	{				
		StringBuilder items = new StringBuilder();
		items.append("[ ");
		Node<E> currentQueue = front;
		//Iterate over the collection and display the item 
		while(currentQueue != null)
		{
			items.append(currentQueue.getItem(items));
			items.append(" ");
			currentQueue = currentQueue.next; 
		}
		items.append("]");
		return items.toString();
	}
}