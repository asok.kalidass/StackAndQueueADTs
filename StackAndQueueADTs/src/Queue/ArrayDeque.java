package Queue;
import java.util.Arrays;
import Exceptions.*;
import Interface.Deque;
/**
 * This Generic class implements Deque interface and contains various methods for manipulating Deque using Generic Array (such as
 * two way insert and delete).
 * <p>
 * Implementation of Deque may have following Special Case
 * <ul>
 * <li>First Element Insertion - cannot insert from front end if index[0] is not null.
 * <li>Last Element Deletion - The front and rear counter reset to -1 upon deletion of last element
 * <li>Empty Array - Denoted by rear and front pointer is -1
 * <li>When Rear and Front pointer points to same index it implies array size is 1
 * <li>Array Filled - Denoted by pointers having value, array size minus one 
 * </ul>
 * <p>
 * Assumptions/Restrictions: User inputs valid single dimension array of allowable length
 * <p>
 * Noteworthy Features: 
 * <ul>
 * <li>Resizable 1-D array implementation of Interface Deque
 * <li>Tested using Junit for various scenarios 
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public class ArrayDeque<E> implements Deque<E> {
	//Declarations
	private E array[];
	int size = 0, front = -1, rear = -1, maxSize = 0;
	@SuppressWarnings("unchecked")

	/**
	 * Initializes an empty array of fixed size
	 * @param maxSize - size of an array
	 */
	public ArrayDeque(int maxSize) {
		this.array = (E[]) new Object[maxSize];
		this.maxSize = maxSize;
	}
	/**
	 * Returns the size of an array
	 * @return Returns the size of an array
	 */
	@Override
	public int size() {		
		return size;
	}
	/**
	 * Checks for an empty array
	 * @return True - if empty else false
	 */
	@Override
	public boolean isEmpty() {
		//-1 denotes the pointers are not pointing any index (implies no element in an array)
		if ((front == -1) && (rear == -1)) {
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Inserts an element at front of deque
	 * @param element - data to be added
	 * @throws FullStructureException when deque is full
	 */
	@Override
	public void insertFirst(E element) {
		//Starting condition i.e queue is empty
		if (front == -1) { 
			front++;
			array[front] = element; 			
		}
		//No space on the left side since element at 0 is already filled
		else if (front == 0 && array[front] != null) { 
			throw new FullStructureException("No Space to insert at front End");
		}	
		else {
			array[front] = element;
			front--;
		}	
		size++;
	}
	/**
	 * Inserts an element at rear end of the deque
	 * @param element - data to be added
	 * @throws FullStructureException when deque is full
	 */
	@Override
	public void insertLast(E element) {	
		//Starting condition i.e queue is empty
		if (rear == -1 && array[0] != null) { 
			rear++;
		}
		// No space to push element at its tail since element at last index is already filled
		if (rear == maxSize -1) { 
			throw new FullStructureException("No Space to insert at rear End");
		}
		rear++;
		array[rear] = element; 
		size++;
	}
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	@Override
	public E removeFirst() throws EmptyStructureException {
		E currentVal =  null;
		if (isEmpty()) {
			throw new EmptyStructureException();
		}
		if (front >= -1) {
			front++;
			currentVal = array[front];
			array[front] = null;
			size--;
		}
		//After Last element deletion reset the front and rear to -1 to denote empty position
		if (size == 0) { 
			front = rear = -1;
		}	
		return currentVal;
	}	
	/**
	 * Remove an element from front of deque
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	@Override
	public E removeLast() throws EmptyStructureException {
		E currentVal = null;
		if (isEmpty()) {
			throw new EmptyStructureException();
		}
		if (rear >= -1) {				
			currentVal = array[rear];
			array[rear] = null;	
			rear--;
			size--;
		}	
		//After Last element deletion reset the front and rear to -1 to denote empty position
		if (size == 0) { 
			front = rear = -1;
		}	
		return currentVal;
	}
	/**
	 * Returns the array content 
	 * @return Returns the array content 
	 */
	@Override
	public String toString() {
		return "ArrayDeque [array=" + Arrays.toString(array) + "]";
	}
	/**
	 * Returns First element of deque
	 * @return Return First element of deque
	 */
	@Override
	public E firstElement() throws EmptyStructureException {
		return array[front];
	}
	/**
	 * Returns last element of deque
	 * @return Return last element of deque
	 */
	@Override
	public E lastElement() throws EmptyStructureException {
		return array[rear];
	}
}
