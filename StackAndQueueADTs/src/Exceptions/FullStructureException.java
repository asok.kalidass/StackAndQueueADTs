package Exceptions;
/**
 *Represents FullStructureException thrown upon manipulation full structure 
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class FullStructureException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	/*
	 * Initializes FullStructureException and calls base class constructor
	 */
	public FullStructureException() { 
		super(); 
	}
	/*
	 * Initializes FullStructureException and calls base class constructor with passed string
	 */
	public FullStructureException(String s) { 
		super(s); 
		System.out.println(s);
	}
}
