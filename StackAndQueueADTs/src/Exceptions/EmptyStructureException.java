package Exceptions;
/**
 *Represents EmptyStructureException thrown upon manipulation empty structure 
 *@author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 */
public class EmptyStructureException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	/*
	 * Initializes EmptyStructureException and calls base class constructor
	 */
	public EmptyStructureException() { 
		super(); 
	}
	/*
	 * Initializes EmptyStructureException and calls base class constructor with passed string
	 */
	public EmptyStructureException(String s) { 
		super(s); 
		System.out.println(s);
	}
}
