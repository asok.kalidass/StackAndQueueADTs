package Stack;

import Exceptions.EmptyStructureException;
import Interface.Deque;
/**
 * This Generic class implements Deque interface and contains various methods for manipulating Stack using Linked List.
 * <p>
 * Implementation of stack using linked list may have following Special Case
 * <ul>
 * <li>First Element Insertion - Both Front and Rear points to same node
 * <li>Empty Array - Denoted by size zero
 * <li>When Rear and Front pointer points to same index it implies list has only one value
 * </ul>
 * <p>
 * Assumptions/Restrictions: User inputs valid single dimension array of allowable length
 * <p>
 * Noteworthy Features: 
 * <ul>
 * <li>Generic Implementation of linked list
 * <li>Tested using Junit for various scenarios 
 * <li>The order of iterating element over array takes constant time since the head position always at starting point of list ( insert/delete from left side )
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public class LinkedListStack<E> implements Deque<E> {
	//Declarations
	int size = 0;
	private Node<E> top;

	/**
	 * Initializes an Linked Lists
	 * @param maxSize
	 */
	public LinkedListStack() {	

	}
	/**
	 * This class represent the nodes of a linked list
	 *
	 * @param <E> Element to be inserted to a list
	 */
	@SuppressWarnings("hiding")
	class Node<E>
	{ 
		E item;
		Node<E> next;
		/*
		 * Initializes a Node 
		 */
		Node() {

		}		
		public E getItem(StringBuilder items)
		{
			return item;
		}
	}
	/**
	 * Returns the size of an array
	 * @return Returns the size of an array
	 */
	@Override
	public int size() {		
		return size;
	}
	/**
	 * Checks for an empty array
	 * @return True - if empty else false
	 */
	@Override
	public boolean isEmpty() {			
		//0 denotes the size variable is not manipulated at any of the below methods implying there are no elements in an array)
		if ((size == 0)) {
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Inserts an element at front of stack
	 * @param element - data to be added
	 */
	@Override
	public void insertFirst(E element) {
		Node<E> frontElement = new Node<E>();
		frontElement.item = element;
		frontElement.next = top;
		top = frontElement;				
		size++;
	}
	/**
	 * Remove an element from front of stack
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	@Override
	public E removeFirst() throws EmptyStructureException {
		Node<E> remapFirstNode = top;		
		if (top != null) {			
			top = remapFirstNode.next;
			size--;
		}
		else {
			throw new EmptyStructureException("Atleast one item is required to perform deletion operation");
		}
		return remapFirstNode.item;
	}	
	/**
	 * Returns First element of stack
	 * @return Return First element of stack
	 */
	@Override
	public E firstElement() throws EmptyStructureException {
		//Always front points to head position of stack
		Node<E> frontNode = top ;
		return frontNode.item;
	}
	/**
	 * Returns the node content 
	 * @return Returns the node content 
	 */
	@Override
	public String toString() {
		return readStack();
	}
	/*
	 * Reads the queue
	 */
	public String readStack()
	{				
		StringBuilder items = new StringBuilder();
		items.append("[ ");
		Node<E> currentQueue = top;
		//Iterate over the collection and display the item 
		while(currentQueue != null)
		{
			items.append(currentQueue.getItem(items));
			items.append(" ");
			currentQueue = currentQueue.next; 
		}
		items.append("]");
		return items.toString();
	}
	@Override
	public void insertLast(E element) {
		// TODO Auto-generated method stub

	}
	@Override
	public E removeLast() throws EmptyStructureException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E lastElement() throws EmptyStructureException {
		// TODO Auto-generated method stub
		return null;
	}
}
