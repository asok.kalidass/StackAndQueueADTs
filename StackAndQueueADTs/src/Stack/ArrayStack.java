package Stack;
import java.util.Arrays;
import Exceptions.*;
import Interface.Deque;

/**
 * This Generic class implements Deque interface and contains various methods for manipulating stack using Generic Array (such as
 * two way insert and delete).
 * <p>
 * Implementation of stack may have following Special Case
 * <ul>
 * <li>First Element at 'last' index is considered as Bottom of the array.
 * <li>Last element deletion - Reset the top counter to its default position i.e, last index and reset the size to '0' to denote empty stack
 * <li>Empty Stack - Denoted by size counter
 * </ul>
 * <p>
 * Assumptions/Restrictions: User inputs valid single dimension array of allowable length
 * <p>
 * Noteworthy Features: 
 * <ul>
 * <li>Resizable 1-D array implementation of Interface Deque
 * <li>Tested using Junit for various scenarios
 * <li>The order of iterating element over array takes constant time since the head position always at top of stack 
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356) - Graduate Student
 *
 * @param <E> Data type of elements in the collection
 */
public class ArrayStack<E> implements Deque<E> {
	//Declarations
	private E array[];
	//top points to the current position of an element in a stack
	int size = 0, top = 0,  maxSize = 0;
	/**
	 * Initializes an empty array of fixed size
	 * @param maxSize - size of an array
	 */
	@SuppressWarnings("unchecked")
	public ArrayStack(int maxSize) {
		this.array = (E[]) new Object[maxSize];
		this.maxSize = maxSize;
		this.top = maxSize - 1;
	}
	/**
	 * Returns the size of an array
	 * @return Returns the size of an array
	 */
	@Override
	public int size() {
		return size;
	}
	/**
	 * Checks for an empty array
	 * @return True - if empty else false
	 */
	@Override
	public boolean isEmpty() {
		if (size == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	/**
	 * Remove an element from front of stack - POP Operation
	 * The first index of an array is the mouth of a stack
	 * @return element that was removed
	 * @throws EmptyStructureException when array index has no empty
	 */
	@Override
	public E removeFirst() throws EmptyStructureException {
		E removedElement = null;
		if (isEmpty()) {
			throw new EmptyStructureException("No elements in the stack to pop out");
		}
		else {
			removedElement = array[top + 1];
			array[top + 1] = null;
			top += 1;
			size--;
		}		
		//After Last element deletion reset the top counter to last index of array to denote base position
		if (size == 0) { 
			top = maxSize - 1;
		}	
		return removedElement;
	}		
	@Override
	public E removeLast() throws EmptyStructureException {		
		return null;
	}
	/*
	 * Returns Last element of an array 
	 * @returns - top element in a stack
	 * @throws EmptyStructureException when stack is empty
	 * @see Interface.Deque#lastElement()
	 */
	@Override
	public E lastElement() throws EmptyStructureException {
		return null;
	}
	/**
	 * Inserts an element at rear end of the stack - PUSH operation
	 * @param element - data to be added
	 * @throws FullStructureException when stack is full
	 */
	@Override
	public void insertLast(E element) {		
	}
	/*Returns array as string 
	 * @return Returns array content
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ArrayStack [array=" + Arrays.toString(array) + "]";
	}
	/**
	 * Inserts an element at front end of the stack - PUSH operation
	 * @param element - data to be added
	 * @throws FullStructureException when stack is full
	 */
	@Override
	public void insertFirst(E element) {
		// No space to push element at its tail since element at last index is already filled
		if (size == maxSize) { 
			throw new FullStructureException("No Space to push at rear End");
		}
		array[top] = element; 		
		top--;
		size++;
	}	
	/*
	 * Returns first element of an array - TOP operation
	 * @returns - top element in a stack
	 * @throws EmptyStructureException when stack is empty
	 * @see Interface.Deque#firstElement()
	 */
	@Override
	public E firstElement() throws EmptyStructureException {
		if (isEmpty()) {
			throw new EmptyStructureException("No elements in the stack to top out");
		}
		else {
			return array[top + 1];
		}
	}	
}
